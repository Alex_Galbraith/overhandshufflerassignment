package week09.random_deck;
import week09.overhand_shuffler.*;
import java.io.*;
import java.util.Random;
/**
 *RandomAppOverhand constructs a OverhandShuffler, then calls a
 *set ammount of shuffles on a certain ammount of decks of a certain
 *size in main.
 *
 *
 *@author Oscar Allen
 */
class RandomAppOverhand{
    /**
     *os is an OverhandShuffler that has its methods called
     *through system in to compare its effeciency against
     *the genuine RandomApp 
     */
   static  OverhandShuffler os= new OverhandShuffler();
    /**
     *The main function on the randomAppOverhand is designed to
     *take system input and then calls a
     *set ammount of shuffles on a certain ammount of decks of a certain
     *size in main.
     *
     *@param args . Args are the system input, so that commands can be
     *run from what the system inputs isntead of changing the file to
     *test diffrent parameters
     */
    public static void main(String[] args){
        int size=50;
        int count=10000;
        int shuff=80;
        switch(args.length){
            case 0:
                break;
            case 1:
                try{
                    size=Integer.parseInt(args[0]);
                }catch(NumberFormatException e){
                    System.out.println("Could not read size parameter."
                                       +"Defaulting to size 50.");
                }
                break;
            case 2:
                try{
                    size=Integer.parseInt(args[0]);
                    count=Integer.parseInt(args[1]);
                }catch(NumberFormatException e){
                    System.out.println("Poorly formed paramerters. "
                                       +"Should be two integers.");
                }
                break;
            case 3:
                try{
                    size=Integer.parseInt(args[0]);
                    count=Integer.parseInt(args[1]);
                    shuff=Integer.parseInt(args[2]);
                }catch(NumberFormatException e){
                    System.out.println("Poorly formed paramerters. "
                                       +"Should be three integers.");
                }
                
                break;
            case 4:
                System.out.println("Should be of the form RandomApp [size]"
                                   +"[count] [shuffles]");
                break;
        }
        generateData(size,count,shuff);

    }
    /**
     * Generates 'count' random decks of size 'size'. Checks how many
     * unbroken pairs are found in each deck. Outputs these raw values to
     * outputRaw.csv. Groups the results together and outputs this to
     * output.csv. This grouping is such that 0, 1, 0, 0, 1 would be grouped
     * to 3, 2, 0. I.E., three zeroes, two ones, and zero twos.
     * @param size Size of the decks to generate
     * @param count How many decks to generate and test.
     */
    public static void generateData(int size, int count,int shuffleCount){
        int[] sums= new int[size];
        try{
            File fl=new File("outputOverhand.csv");
            File flRaw= new File("outputOverhandRaw.csv");
            fl.createNewFile();
            flRaw.createNewFile();
            FileWriter f=new FileWriter(fl);
            FileWriter fRaw=new FileWriter(flRaw);
            for(int i=0;i<count;i++){
                os.makeNew(size);
                for(int a=0;a<shuffleCount;a++){
                    os.randomShuffle();
                }
                int result=os.unbrokenPairsSimple();
                fRaw.write(""+result+", ");
                sums[result]++;
            
            }
            for(int i:sums){
                //loop over sums and print to file
                f.write(""+i+", ");
            }
            f.close();
            fRaw.close();
        }catch(IOException e){
            System.out.println("ARRRGH");
        }
    }


}
