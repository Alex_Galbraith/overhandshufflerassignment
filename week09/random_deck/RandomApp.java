package week09.random_deck;
import week09.overhand_shuffler.*;
import java.io.*;
import java.util.Random;
/**
 * This class is used to generate data about randomized decks. It writes the
 * number of unbroken pairs in both raw format and grouped format to
 * outputRaw.csv and output.csv repectively.
 * @author Alex Galbraith
 */ 
class RandomApp{
    /**
     * Main function to produce results.
     * @param args Two int arguments are excepted, the first being the size
     * of the deck, the second being the number of trials.
     */
    public static void main(String[] args){
        int size=50;
        int count=10000;
        switch(args.length){
            case 0:
                break;
            case 1:
                try{
                    size=Integer.parseInt(args[0]);
                }catch(NumberFormatException e){
                    System.out.println("Could not read size parameter."
                                       +"Defaulting to size 50.");
                }
                break;
            case 2:
                try{
                    size=Integer.parseInt(args[0]);
                    count=Integer.parseInt(args[1]);
                }catch(NumberFormatException e){
                    System.out.println("Poorly formed paramerters. "
                                       +"Should be two integers.");
                }
                break;
            case 3:
                System.out.println("Should be of the form RandomApp [size]"
                                   +"[count]");
                break;
        }
        generateData(size,count);

    }
    /**
     * Generates 'count' random decks of size 'size'. Checks how many
     * unbroken pairs are found in each deck. Outputs these raw values to
     * outputRaw.csv. Groups the results together and outputs this to
     * output.csv. This grouping is such that 0, 1, 0, 0, 1 would be grouped
     * to 3, 2, 0. I.E., three zeroes, two ones, and zero twos.
     * @param size Size of the decks to generate
     * @param count How many decks to generate and test.
     */
    public static void generateData(int size, int count){
        int[] sums=new int[size];
        try{
            File f=new File("output.csv");
            File fRaw=new File("outputRaw.csv");
            f.createNewFile();
            fRaw.createNewFile();
            FileWriter fr=new FileWriter(f);
            FileWriter frRaw=new FileWriter(fRaw);
            for(int i=0;i<count;i++){
                int[] deck= generateRandomDeck(size);
                int result=OverhandShuffler.unbrokenPairsSimple(deck);
                frRaw.write(""+result+", ");
                sums[result]++;
            
            }
            for(int i:sums){
                //loop over sums and print to file
                fr.write(""+i+", ");
            }
            fr.close();
            frRaw.close();
        }catch(IOException e){
            System.out.println("Something went horribly wrong: "
                               +e.getMessage());
        }
    }
    /**
     * Generates a random deck of size 'size'. The deck is randomized by
     * randomly swapping ints inside the deck.  
     * @param size size of the deck to be generated.
     * @return random deck of size 'size'.
     */
    public static int[] generateRandomDeck(int size){
        int[] deck=new int[size];
        for(int i=0;i<size;i++){
            deck[i]=i;
        }
        shuffle(deck);
        return deck;
    }
    /**
     * Random object used to generate random decks.
     */
    private static final Random R = new Random();
    /**
     * Randomly shuffles the given array of integers. From Otago COSC241
     * lecture 10 slide 10. 
     * @param a Array of integers to suffle.
     */
    public static void shuffle(int[] a) {
        for (
             int i = a.length-1; i > 0; i--) {
            swap(a, i, R.nextInt(i+1));
        }
    };
    /**
     * Swaps the value at the two specified indexes in the specified
     * array. From Otago COSC241
     * lecture 10 slide 10. 
     * @param a Array of integers to swap in.
     * @param i first index to swap.
     * @param j second index to swap.
     */
    private static void swap( int[] a, int i, int j) {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
