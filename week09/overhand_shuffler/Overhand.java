package week09.overhand_shuffler;
/**
 * Represents an object that performs an overhand shuffle on a 'deck' of
 * integers.
 * @author Computer Science department Otago University. Commented by Alex
 * Galbraith.
 **/
public interface Overhand {
    /**
     * Make a new 'deck' consisting of size cards numbered from 0 up to size-1
     *  from top to bottom.
     *  @param size Number of cards in the new deck
     **/
    public void makeNew(int size);
    /**
     * Returns the current state of the deck as an int[]. Modifying this array
     * should not affect the internal state of the Overhand object.
     * @return state of the deck as an int[]
     **/
    public int[] getCurrent();
    /**
     * Shuffles the current state of the deck according to the array of block
     * sizes given.
     * @param blocks An array specifying the size and number of blocks to
     * shuffle in. E.G., {2,3,1} would shuffle 3 blocks, with the first
     * containing two 'cards'(ints), the second containing three, and the third
     * containing one.
     **/
    public void shuffle(int[] blocks);
    /**
     * Returns the minimum number of times that the deck could be shuffled
     * (from its initial state) using the same set of block sizes each
     * time (as given by its argument) in order to return the deck to its
     * initial state.
     * @param blocks An array specifying the size and number of blocks to
     * shuffle in. E.G., {2,3,1} would shuffle 3 blocks, with the first
     * containing two 'cards'(ints), the second containing three, and the third
     * containing one.
     * @return The number of reshuffles using the specified set of blocks that
     * would be required to return the deck to int's initial state. 
    **/
    public int order( int[] blocks);
    /**
     * Returns the number of pairs of cards which were con-secutive in the
     * original deck, and are still consecutive (and in the same order) in
     * the current state of the deck.
     * @return The number of pairs that are still in the original order.
     **/
    public int unbrokenPairs();
}
