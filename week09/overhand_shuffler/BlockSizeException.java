package week09.overhand_shuffler;

/**
 * Exception thrown when an overhand shuffle is called with invalid block sizes.
 * This can occur when either a) one or more block sizes is negative, or b)
 * when the sum of the block sizes does not add up to the whole deck size.
 **/
public class BlockSizeException extends RuntimeException{
  public BlockSizeException(String s){
    super(s);
  }
};
