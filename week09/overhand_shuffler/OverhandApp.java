package week09.overhand_shuffler;
import java.util.Arrays;
import week09.command.*;
import java.util.Scanner;
/**
*OverhandApp Constructs both a OverhandShuffler and a CommandManager,
*Then uses a scanner to scan system input and execute any commands entered
*
*/
public class OverhandApp{

    public static void main(String[] args){
        OverhandShuffler os=new OverhandShuffler();
        CommandManager manager=new CommandManager();
        manager.addCommand(new MakeNewCommand(os));
        manager.addCommand(new PrintCommand(os));
        manager.addCommand(new ShuffleCommand(os));
        manager.addCommand(new OrderCommand(os));
        manager.addCommand(new CountShuffleCommand(os));
        manager.addCommand(new RandomShuffleCommand(os));
        manager.addCommand(new UnbrokenPairsCommand(os));
        manager.addCommand(new LoadCommand(os));
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNextLine()){
          manager.parseInputLine(scanner.nextLine());
        }

    }
}
