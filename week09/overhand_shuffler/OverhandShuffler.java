package week09.overhand_shuffler;
import java.util.Arrays;
import java.util.Random;
import java.util.ArrayList;
public class OverhandShuffler implements Overhand{
    private int[] originalDeck;
    private int[] deck;
    private Random r;
    /**
     * Gets the random number generator. If the random number generator is currently null, a new
     * Random will be instantiated.
     * @return r Random number generator.
     * */
    private Random getRandom(){
      if(r==null){
        r=new Random();
      }
      return r;
    }
    /**
     * Sets the random number generator.
     * @param r instance of java.util.Random 
     * */
    public void setRandom(Random r){
      this.r=r;
    }
    
    /**
     * Make a new 'deck' consisting of size cards numbered from 0 up to size-1
     *  from top to bottom.
     *  @param size Number of cards in the new deck
     **/
    public void makeNew(int size){
        deck=new int[size];
        for(int i=0;i<size;i++){
            deck[i]=i;
        }
        //save the state of the original deck
        originalDeck=deck.clone();
    }
    /**
     * Returns the current state of the deck as an int[]. Modifying this array
     * should not affect the internal state of the Overhand object.
     * @return state of the deck as an int[]
     **/
    public int[] getCurrent(){
        //Shallow clone the array so the internal state cannot be modified.
        return deck.clone();
    }
    /**
     * Shuffles the specified deck according to the array of block
     * sizes given.
     * @param deck Array of ints to shuffle.
     * @param blocks An array specifying the size and number of blocks to
     * shuffle in. E.G., {2,3,1} would shuffle 3 blocks, with the first
     * containing two 'cards'(ints), the second containing three, and the third
     * containing one.
     * @return newly shuffle int array. 
     **/
    public static int[] shuffle(int[] deck,int[] blocks){
        //TODO: depends how this error is supposed to be handled 
        if(!checkBlocksValid(blocks,deck.length)){
            throw new BlockSizeException("Invalid breaks. Either the sum of breaks is not equal to the size of the"
                                           +" deck, or a break is negative.");
        }
        int index=0;
        int[] newDeck=new int[deck.length];
        for(int blockNum=0;blockNum<blocks.length;blockNum++){
            for(int i=0;i<blocks[blockNum];i++){
                newDeck[index+i]=deck[(deck.length)-(index+blocks[blockNum])+i];
            }
            index+=blocks[blockNum];
        }
        return newDeck;
    }
    /**
     * Shuffles the current state of the deck according to the array of block
     * sizes given. Calls underlying shuffle(int[] deck,int[] blocks) with deck as the class data field
     * deck.
     * @param blocks An array specifying the size and number of blocks to
     * shuffle in. E.G., {2,3,1} would shuffle 3 blocks, with the first
     * containing two 'cards'(ints), the second containing three, and the third
     * containing one.
     **/
    public void shuffle(int[] blocks){
      if(deck==null){
        return;
      }
      deck=shuffle(deck,blocks);
    }
    /**
     * Returns the minimum number of times that the deck could be shuffled
     * (from its initial state) using the same set of block sizes each
     * time (as given by its argument) in order to return the deck to its
     * initial state.
     * @param blocks An array specifying the size and number of blocks to
     * shuffle in. E.G., {2,3,1} would shuffle 3 blocks, with the first
     * containing two 'cards'(ints), the second containing three, and the third
     * containing one.
     * @return The number of reshuffles using the specified set of blocks that
     * would be required to return the deck to int's initial state. 
     **/
    public int order( int[] blocks){
        int count=0;
        int[] old=deck.clone();
        do{
            shuffle(blocks);
            count++;
        }
        while(!Arrays.equals(old,deck));
        return count;
    }
    /**
     * Calls unbrokenPairsSimple method on deck in order to check to
     * see how many pairs of cards
     * are consecutive in the current state of deck.
     **/
    public  int unbrokenPairsSimple(){
        return unbrokenPairsSimple(deck);
    }

    /**
     * Returns the number of pairs of cards which are
     * consecutive in an ordered deck,
     * and are consecutive (and in the same order) in
     * the current state of the deck.
     * @return The number of pairs that are still in a consecutive order.
     **/
    public static int unbrokenPairsSimple(int[] deck){
        int count=0;
        for(int i=1;i<deck.length;i++){
            int checkNum=deck[i];
            int otherNum=deck[i-1];
            if(checkNum==otherNum+1){
                count++;
            }
        }
        return count;
    }
    
    /**
     * Calls unbrokenPairs method on deck in order to check to see how many pairs of cards
     * are consecutive in the current state of deck using a more advanced method.
     **/
    public  int unbrokenPairs(){
        return unbrokenPairs(deck, originalDeck);
    }
 
    
    /**
     * Returns the number of pairs of cards which were consecutive in the
     * original deck, and are still consecutive (and in the same order) in
     * the current state of the deck.
     * @return The number of pairs that are still in the original order.
     **/
    public static int unbrokenPairs(int[] deck, int[] originalDeck){
        int count=0;
        for(int i=1;i<deck.length;i++){
            int checknum=deck[i];
            int othernum=deck[i-1];
            for(int j=1;j<originalDeck.length;j++){
                if(checknum==originalDeck[j] && othernum==originalDeck[j-1]){
                    count++;
                    break;
                }
            }
        }
        return count;
    }
 
    /**
     * Generate a random set of breaks for use in a shuffle. The breaks will be created based on the
     * provided break probability. I.e., a probability of 0.1 will have a one in 10 chance of breaking
     * on any card.
     * @param breakProbability Chance of breaking at any given position. Float between 0 and 1. Values
     * outside this range will be clamped.
     * @return an array of ints representing how many cards are in each break. The sum of these breaks
     * will always be eequal to the number of cards in the current deck.
     **/
    public int[] generateRandomBreaks(float breakProbability){
        breakProbability = Math.max(0, Math.min(1, breakProbability));
        ArrayList<Integer> blocks=new ArrayList<Integer>();
        int lastBlock=0;
        int i=0;
        for(;i<deck.length;i++){
            if(getRandom().nextFloat()<breakProbability){
                //break a block 
                blocks.add(i-lastBlock);
                lastBlock=i;
            }
        }
        //if we reached the end of the loop before doing the final break.
        if(lastBlock!=deck.length){
            blocks.add(i-lastBlock);
        }
        //Shitty conversion from Integer to int
        //Stolen from http://stackoverflow.com/questions/960431/how-to-convert-listinteger-to-int-in-java
        int[] ret = new int[blocks.size()];
        for(i = 0;i < ret.length;i++){
            ret[i] = blocks.get(i);
        }
        return ret;
    }
    /**
     * Performs a random overhand shuffle using a break probability of 0.1.
     * */
    public void randomShuffle(){
      int[] ret=generateRandomBreaks(0.1f);
      shuffle(ret);
    }



    
    /**
     * Counts how many random shuffles that is needed to be done before tue unbroken pairs is less than
     * the given parameter.
     * @return how many random shuffles that is needed to be done before tue unbroken pairs is less than
     * the given parameter.
     */
    
    public int countShuffles(int unbrokenPairs){
        int count=0;
        makeNew(52);
        while(unbrokenPairsSimple()>=unbrokenPairs){
            randomShuffle();
            count++;
        }
        System.out.println("unBroken Pairs: "+unbrokenPairs());
        System.out.println("Finished shuffling, less unbroken pairs than "+unbrokenPairs+" with "+count+" shuffles");
        return count;
        
        
    }
    
    
    /**
     * This is a function that proves we can predict what the state of the
     * deck would be if we repeated a sequence of shuffles, regardless of whether we know
     * what the individual shuffles themselves were. This function does not modify the underlying state
     * of the deck.
     **/
    public void tryRepeat(){
        int shuffleNum=getRandom().nextInt(3)+2;
        int[][] blocks = new int[shuffleNum][];
        System.out.println("/////BEGIN tryRepeat TEST/////");
        System.out.println("Unshuffled: "+Arrays.toString(deck));
        int[] fullShuffle=deck.clone();//propper double shuffle
        int[] repeatShuffle;//attempted repeat deck
        //first shuffle
        for(int i=0;i<shuffleNum;i++){
            blocks[i]=generateRandomBreaks(0.1f);
            System.out.println("Block: "+i+" :"+Arrays.toString(blocks[i]));
            fullShuffle=shuffle(fullShuffle,blocks[i]);
        }
        //map where each card has gone
        int[] map=new int[deck.length];
        for(int i=0;i<deck.length;i++){
            int currentCard=deck[i];
            for(int j=0;j<deck.length;j++){
                if(fullShuffle[j]==currentCard){
                    map[i]=j;
                }
            }
        }
        System.out.println("First shuffle: "+Arrays.toString(fullShuffle));
        System.out.println("Mapping unshuffled to shuffled: "+Arrays.toString(map));

        repeatShuffle=new int[deck.length];
        for(int i=0;i<deck.length;i++){
            repeatShuffle[map[i]]=fullShuffle[i];
        }      
        
        //second shuffle
        for(int i=0;i<shuffleNum;i++){
            fullShuffle=shuffle(fullShuffle,blocks[i]);
        }
        System.out.println("Repeated shuffle without knowing breaks: "+Arrays.toString(repeatShuffle));
        System.out.println("Manual repeated shuffle: "+Arrays.toString(fullShuffle));
        if(Arrays.equals(repeatShuffle,fullShuffle)){
            System.out.println("It works!");
        }else{
            System.out.println("Something went very wrong...");
        }
        System.out.println("/////END tryRepeat TEST/////");
    }
    /**
     * Checks to ensure the given blocks are not invalid.
     * Invalidity occurs when either a) one or more block sizes is negative,
     * or b) when the sum of the block sizes does not add up to the whole deck
     * size.
     * @param blocks set of blocks to check.
     * @return true if blocks are valid, false otherwise.
     **/
    private boolean checkBlocksValid(int[] blocks){
        int sum=0;
        for(int i=0;i<blocks.length;i++){
            if(blocks[i]<0){
                return false;
            }
            sum+=blocks[i];
        }
        if(sum!=deck.length){
            return false;
        }
        return true;
    }
    /**
     * Checks to ensure the given blocks are not invalid.
     * Invalidity occurs when either a) one or more block sizes is negative,
     * or b) when the sum of the block sizes does not add up to the whole deck
     * size.
     * @param blocks set of blocks to check.
     * @param length of the deck this set of blocks will be shuffled on
     * @return true if blocks are valid, false otherwise.
     **/
    private static boolean checkBlocksValid(int[] blocks,int length){
        int sum=0;
        for(int i=0;i<blocks.length;i++){
            if(blocks[i]<0){
                return false;
            }
            sum+=blocks[i];
        }
        if(sum!=length){
            return false;
        }
        return true;
    }
    /**
     * Loads the deck using the given numbers. No error checking is performed.
     * @param cards int array representing the cards to load into the deck.
     * */
    public void load(int[] cards){
      deck=cards.clone();
    }
    /**
     * Returns the deck as a string representation of an array of ints. 
     * Each int represents a card in the deck.
     * @return the deck as a string representation of an array of ints. 
     * */
    public String toString(){
      return Arrays.toString(deck);
    }

    
}
