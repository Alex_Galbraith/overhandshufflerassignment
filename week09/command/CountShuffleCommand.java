package week09.command;

/**
 * Implements a command that runs OverhandShuffler.CountShuffles(int size).
 * Takes an instance of OverhandShuffler as a constructor argument.
 * Based on code from Alex Galbraith 
 * @author Oscar Allen
 **/
import week09.overhand_shuffler.*;

public class CountShuffleCommand implements CommandExecutor{
    private OverhandShuffler os;
    /**
     * Creates a new MakeNewCommand which will act apon the
     * specified OverhandShuffler.
     * @param os OverhandShuffler object to create new decks inside of.
     * */
    public CountShuffleCommand(OverhandShuffler os){
        this.os=os;
    }
    /**
     * Makes a new deck inside the overhandShuffler
     * specified upon construction of this object. Expects one argument
     * containing a string representation of an integer>0.
     * @param args Should contain a single integer represented as a string.
     * Extra arguments will be ignored.
     * @throws IllegalArgumentException if a valid (integer>0) size
     * is not parsed as the first argument.
     * */
    public void execute(String[] args) throws IllegalArgumentException{
        if (args.length==0){
            throw new IllegalArgumentException("Command should be of the form:"
                                               +"\"count-shuffle <unbrokenPairs>\"");
        }
        int size=0;
        try{
            size=Integer.parseInt(args[0]);
        }catch(NumberFormatException e){
            throw new IllegalArgumentException("The size you entered is not an"
                                               +"integer.");
        }
        if(size<=0){
            throw new IllegalArgumentException("Size must be greater than zero.");
        }
        os.countShuffles(size);
    }
    /**
     * Returns an array of tokens by which this command can be called:
     *"count-shuffle""c"
     * @return "count-shuffle""c"
     * */
    public String[] getMnemonics(){
        return new String[]{"count-shuffle","c"};
    }
}
