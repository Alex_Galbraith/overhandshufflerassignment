package week09.command;

/**
 * Implements a command that runs OverhandShuffler.shuffle(int[] blocks).
 * Takes an instance of OverhandShuffler as a constructor argument.
 * @author Alex Galbraith
 **/

import week09.overhand_shuffler.*;

public class ShuffleCommand implements CommandExecutor{
  private OverhandShuffler os;
  /**
   * Creates a new PrintCommand which will act apon the specified
   * OverhandShuffler.
   * @param os OverhandShuffler object to shuffle on.
   * */
  public ShuffleCommand(OverhandShuffler os){
    this.os=os;
  }
   /**
   * Shuffles the deck according to the series of breaks provided as argument.
   * Calls shuffle on the OverhandShuffler
   * object provided at construction.
   * @param args Should be a list of integers>0 as strings that sum to the
   * size of the deck.
   * @throws IllegalArgumentException if a valid set of breaks are not provided.
   * */
  public void execute(String[] args)throws IllegalArgumentException{
    if (args.length==0){
      throw new IllegalArgumentException("Command should be of the form:"
                                         +"\"shuffle <int_1> <int_2> ..."
                                         +"<int_n>\"");
    }
    int[] blocks=new int[args.length];
    
    for (int i=0;i<args.length;i++){
      try{
        blocks[i]=Integer.parseInt(args[i]);
      }catch(NumberFormatException e){
        throw new IllegalArgumentException("Break "+i+" ("+args[i]+") is not "
                                           +"an integer");
      }
    }
    try{
      os.shuffle(blocks);
      System.out.println(os);
    }catch(BlockSizeException e){
      throw new IllegalArgumentException(e.getMessage());
    }
  }
  /**
   * Returns an array of tokens by which this command can be called:
   * "shuffle","s"
   * @return "shuffle","s"
   * */
  public String[] getMnemonics(){
    return new String[]{"shuffle","s"};
  }
}
