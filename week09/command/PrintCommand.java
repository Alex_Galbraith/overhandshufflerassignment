package week09.command;

/**
 * Implements a command that prints an OverhandShuffler to System.out.
 * Takes an instance of OverhandShuffler as a constructor argument.
 * @author Alex Galbraith
 **/

import week09.overhand_shuffler.*;

public class PrintCommand implements CommandExecutor{
  private OverhandShuffler os;
  /**
   * Creates a new PrintCommand which will print the specified
   * OverhandShuffler.
   * @param os OverhandShuffler object to print.
   * */
  public PrintCommand(OverhandShuffler os){
    this.os=os;
  }
   /**
   * Prints the OverhandShuffler specified upon construction of this object.
   * Expects no arguments, but
   * ignores extras.
   * @param args Arguments to be handled by the implementation.
   * */
  public void execute(String[] args){
    System.out.println(os);
  }
  /**
   * Returns an array of tokens by which this command can be called: "print","p"
   * @return "print","p"
   * */
  public String[] getMnemonics(){
    return new String[]{"print","p"};
  }
}
