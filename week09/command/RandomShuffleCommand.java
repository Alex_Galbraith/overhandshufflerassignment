package week09.command;

/**
 * Implements a command that runs OverhandShuffler.randomShuffle().
 * Takes an instance of OverhandShuffler as a constructor argument.
 * @author Alex Galbraith
 **/

import week09.overhand_shuffler.*;

public class RandomShuffleCommand implements CommandExecutor{
  private OverhandShuffler os;
  /**
   * Creates a new RandomShuffleCommand which will act apon the
   * specified OverhandShuffler.
   * @param os OverhandShuffler object to random shuffle on.
   * */
  public RandomShuffleCommand(OverhandShuffler os){
    this.os=os;
  }
   /**
   * Calls randomShuffle on the OverhandShuffler specified at construction.
   * @param args No arguments required.
   * */
  public void execute(String[] args){
    os.randomShuffle();
    System.out.println("Randomly shuffled to: ");
    System.out.println(os);
  }
  /**
   * Returns an array of tokens by which this command can be called:
   * "random-shuffle","r"
   * @return "random-shuffle","r"
   * */
  public String[] getMnemonics(){
    return new String[]{"random-shuffle","r"};
  }
}
