package week09.command;

/**
 * Implements a command that runs OverhandShuffler.load(int[] nums), where
 * nums is read in from an Array of ints represented as strings.
 * Takes an instance of OverhandShuffler as a constructor argument.
 * @author Alex Galbraith
 **/

import week09.overhand_shuffler.*;

public class LoadCommand implements CommandExecutor{
  private OverhandShuffler os;
  /**
   * Creates a new LoadCommand which will act apon the specified
   * OverhandShuffler.
   * @param os OverhandShuffler object to print.
   * */
  public LoadCommand(OverhandShuffler os){
    this.os=os;
  }
   /**
   * Loads the deck with the specified array of ints.
   * No error checking is performed.
   * @param args Should be a list of integers
   * @throws IllegalArgumentException if any argument is not a String
   * representation of an int.
   * */
  public void execute(String[] args)throws IllegalArgumentException{
    if (args.length==0){
      throw new IllegalArgumentException("Command should be of the form:"
                                         +"\"load <int_1> <int_2> ... "
                                         +"<int_n>\"");
    }
    int[] newDeck=new int[args.length];
    
    for (int i=0;i<args.length;i++){
      try{
        newDeck[i]=Integer.parseInt(args[i]);
      }catch(NumberFormatException e){
        throw new IllegalArgumentException(""+i+" ("+args[i]+") is not"
                                           +"an integer");
      }
    }
    try{
      os.load(newDeck);
      System.out.println(os);
    }catch(BlockSizeException e){
      throw new IllegalArgumentException(e.getMessage());
    }
  }
  /**
   * Returns an array of tokens by which this command can be called: "load","l"
   * @return "load","l"
   * */
  public String[] getMnemonics(){
    return new String[]{"load","l"};
  }
}
