package week09.command;

/**
 * Implements a command that runs OverhandShuffler.unbrokenPairs().
 * Takes an instance of OverhandShuffler as a constructor argument.
 * @author Alex Galbraith
 **/

import week09.overhand_shuffler.*;

public class UnbrokenPairsCommand implements CommandExecutor{
  private OverhandShuffler os;
  /**
   * Creates a new UnbrokenPairsCommand which will act apon the
   * specified OverhandShuffler.
   * @param os OverhandShuffler object count unbroken pairs on.
   * */
  public UnbrokenPairsCommand(OverhandShuffler os){
    this.os=os;
  }
   /**
   * Calls unbrokenPairs on the OverhandShuffler specified at construction.
   * @param args No arguments required.
   * */
  public void execute(String[] args){
    System.out.println("Unbroken pairs: "+os.unbrokenPairs());
  }
  /**
   * Returns an array of tokens by which this command can be called:
   * "unbroken-pairs","u"
   * @return "unbroken-pairs","u"
   * */
  public String[] getMnemonics(){
    return new String[]{"unbroken-pairs","u"};
  }
}
