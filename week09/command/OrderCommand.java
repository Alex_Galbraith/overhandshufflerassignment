package week09.command;

/**
 * Implements a command that runs OverhandShuffler.order(int[] breaks).
 * Takes an instance of OverhandShuffler as a constructor argument. Takes in
 * the breaks as a string of space separated ints.
 * @author Alex Galbraith
 **/

import week09.overhand_shuffler.*;

public class OrderCommand implements CommandExecutor{
  private OverhandShuffler os;
  /**
   * Creates a new OrderCommand which will act apon the specified
   * OverhandShuffler.
   * @param os OverhandShuffler object to print.
   * */
  public OrderCommand(OverhandShuffler os){
    this.os=os;
  }
   /**
   * Calls the order(int[] blocks) on the OverhandShuffler provided at
   * construction.
   * @param args Set of breaks as an array of string.
   * @throws IllegalArgumentException if the set of breaks is invalid.
   * */
  public void execute(String[] args)throws IllegalArgumentException{
    if (args.length==0){
      throw new IllegalArgumentException("Command should be of the form:"
                                         +"\"order <int_1> <int_2> ..."
                                         +"<int_n>\"");
    }
    int[] blocks=new int[args.length];
    
    for (int i=0;i<args.length;i++){
      try{
        blocks[i]=Integer.parseInt(args[i]);
      }catch(NumberFormatException e){
        throw new IllegalArgumentException("Break "+i+" ("+args[i]+") is not "
                                           +"an integer");
      }
    }
    try{
      int count=os.order(blocks);
      System.out.println("Took "+count+" re-shuffles to return to the original "
                         +"order.");
    }catch(BlockSizeException e){
      throw new IllegalArgumentException(e.getMessage());
    }
  }
  /**
   * Returns an array of tokens by which this command can be called:
   * "order","o"
   * @return "order","o"
   * */
  public String[] getMnemonics(){
    return new String[]{"order","o"};
  }
}
