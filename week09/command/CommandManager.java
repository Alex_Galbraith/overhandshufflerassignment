package week09.command;
import java.util.HashMap;
import java.util.Arrays;
/**
 * This class deals with mapping mnemonics to commands, and parsing input lines
 * into mnemonics and arguments. CommandManager also does basic error checking
 * on commands to ensure there is no overlap in mnemonics.
 *@author Alex Galbraith
 */

public class CommandManager{
  private HashMap<String,CommandExecutor> commands=
      new HashMap<String,CommandExecutor>();
  public void addCommand(CommandExecutor ce){
    //Test mnemonics before we start adding them
    for(String mne : ce.getMnemonics()){
      if(commands.containsKey(mne.toLowerCase())){
        throw new IllegalMnemonicException(mne,ce);
      }
    }
    for(String mne : ce.getMnemonics()){
      commands.put(mne.toLowerCase(),ce);
    }
  }
  /**
   * Parses an input line and attempts to execute a command that matches the
   * input. The tokenization of this input
   * is done very simply. Tokenization does not support escapes, -p style
   * parameters, or anything else fancy you can 
   * think of. Parameters are separated simply by spaces.
   * @param s One line of input.
   * */
  public void parseInputLine(String s){
    String[] tokens=s.split(" ");
    if(tokens.length==0)
      return;
    String mnemonic=tokens[0].toLowerCase();
    String[] args=new String[0];
    //capture args
    if(tokens.length>1){
      args=Arrays.copyOfRange(tokens,1,tokens.length);
    }
    CommandExecutor ce=commands.get(mnemonic);
    if(ce==null){
      System.out.println("Command not found.");
    }else{
      try{
        ce.execute(args);
      }catch(IllegalArgumentException e){
         System.out.println(e.getMessage());
      }
    }
  }
    /**
     * Thrown when a mnemonic is already taken by a command. Specifies the
     * mnemonic and the command that is already using it.
     */
    public class IllegalMnemonicException extends RuntimeException{
        public final String mne;
        public final CommandExecutor ce;
        /**
         * Creates a new IllegalMnemonicException and sets it's mnemonic and
         * command executor information.
         * @param mne Mnemonic that was taken as a string.
         * @param ce CommandExecutor that is already using the mnemonic.
         **/
        public IllegalMnemonicException(String mne,CommandExecutor ce){
            this.mne=mne;
            this.ce=ce;
        }
        /**
         * Returns a human readable error stating which mnemonic was
         * taken by what in the format: "Mnemonic already taken: $mnemonic  by
         * class $classname. Mnemonics are NOT case sensitive." 
         +"sensitive.";
         *@return human readable error stating which mnemonic was taken by what.
         *
         */
        @Override
        public String toString(){
            return "Mnemonic already taken: "+mne+" by class "
                +ce.getClass().getSimpleName()+". Mnemonics are NOT case " 
                +"sensitive.";
        }
    }
}
