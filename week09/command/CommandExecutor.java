package week09.command;

/**
 * This interface provides the contract for a command line command. It is the responsibility of the command
 * implementation to handle errors and parsing of arguments.
 * @Author Alex Galbraith
 * */
public interface CommandExecutor{
  /**
   * Execute this command with the given arguments. It is the responsibility of the command
   * implementation to handle errors and parsing of arguments. Please ensure the error thrown is descriptive; the error
   * text will be returned to the user.
   * @param args Arguments to be handled by the implementation.
   * */
  public void execute(String[] args) throws IllegalArgumentException;
  /**
   * Returns a list of tokens by which this command can be called. When a CommandExecutor is added to the 
   * CommandManager using addCommand, these tokens are mapped to this CommandExecutor. These tokens should be unique,
   * otherwise errors will arise. 
   * @return a list of String tokens by which this command can be called.
   * */
  public String[] getMnemonics(); 
}